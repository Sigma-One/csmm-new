# CSMM - Cities: Skylines Mod Manager
A mod management and Steam Workshop interface for Cities:Skylines written in Kotlin/JVM
___
## About
CSMM is a mod manager for the game Cities:Skylines, intended to be an alternative to the official Steam Workshop UI
and to allow easy installation of workshop items to non-Steam copies of the game, such as those from the Epic Games Store.

When feature-complete, it will allow installing and removing items from both a graphical and command line interface, along with managing sets of items for different playthroughs.
It will also include a search function for the Steam Workshop, along with other C:S mod management utilities.

CSMM works by scraping the Steam Workshop for information and using a [3rd party Steam Workshop downloader](steamworkshopdownloader.io) to download item files.
It installs these items into the local game data directory along with a JSON manifest allowing management by CSMM
___
## Current Status
CSMM is currently lacking a significant portion of it's planned features, and is untested on non-Linux platforms. A usable version can be expected later in 2022,
and no release binaries (or jars) will be provided before that. However, in case a CI is set up, the result jars will be published.

Contributions to further CSMM development are greatly appreciated.
___
## Contributing
There are as of now no too strict contribution guidelines or conventions, however if you do wish to contribute, please attempt to use a similar code formatting to existing code.
Generally, indents should be 4 spaces, and public methods or functions should ideally have a documentation comment. Addition of new external dependencies should also be avoided.

If you do make a contribution, please describe your changes in sufficient detail in a merge request, and in case of extra features, explain why they would be useful.
___
## TODOs
```kotlin
// TODO: Handle errors properly instead of naïvely expecting things to work
// TODO: Command line interface
// TODO: Graphical interface with Compose
// TODO: Spicy workshop spider o8O,w,O8o
// TODO: Unit tests maybe?
// TODO: MacOS support
// TODO: Make sure it works on Windows
// TODO: Write more TODOs
// TODO: Optimisations
// TODO: Hug the lead dev
// TODO: Relocate the bug in httpClient.kt
// TODO: More docs
// TODO: Finish readme
// TODO: Log level configuration
// TODO: Release maybe some day, I hope
// TODO: A CI config maybe possibly?
// TODO: Local item collection saving and loading
```