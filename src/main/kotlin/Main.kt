import backend.config.CSMMConfig
import backend.config.ConfigManager
import backend.data.ItemContext
import backend.local.LocalItemManager
import backend.network.downloader.WorkshopDownloader
import backend.network.workshop.SteamWorkshop
import kotlinx.coroutines.runBlocking
import util.Log
import util.UnsupportedOSException
import java.io.FileNotFoundException
import kotlin.system.exitProcess

/*@Composable
@Preview
fun App() {
    var text by remember { mutableStateOf("Hello, World!") }

    MaterialTheme {
        Button(onClick = {
            text = "Hello, Desktop!"
        }) {
            Text(text)
        }
    }
}*/

fun main(): Unit = runBlocking /*application*/ {
    // Ignore this it's just here so I don't forget how to do this
    /*Window(onCloseRequest = ::exitApplication) {
        App()
    }*/

    // Initialise config
    try { ConfigManager.loadConfig() }
    // If config doesn't exist, create it
    catch (e: Throwable) {
        when (e) {
            // No need to handle this branch's UnsupportedOSException, it can't occur here
            is FileNotFoundException  -> ConfigManager.writeOrCreateConfig(CSMMConfig())
            is UnsupportedOSException -> {
                Log.FAIL("Could not load or create config (Unsupported OS), exiting")
                exitProcess(1)
            }
            else -> throw e
        }
    }

    val item1 = ItemContext()
    item1.id = 2115871076L

    val item2 = ItemContext()
    item2.id = 1469499512L

    val i2d = item2.id
    val item2d = LocalItemManager.readManifestForID(i2d!!)

    //WorkshopDownloader.downloadItems(item1, item2)
    //LocalItemManager.listItems().forEach { it.value.forEach { println(it) } }
    //LocalItemManager.removeItem(LocalItemManager.readManifestForID(1433003062L)!!)
    //LocalItemManager.checkForItemUpdates().forEach { println(it) }
    //println(SteamWorkshop.keywordQuery("zjfgkjgkjdasfas", 1))
    println(SteamWorkshop.keywordQuery("car", 2))
}
