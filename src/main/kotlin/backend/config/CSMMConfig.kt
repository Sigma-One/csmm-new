package backend.config

import io.ktor.util.*
import kotlinx.serialization.Serializable
import util.UnsupportedOSException
import java.io.File

/** CSMMConfig
 * @author  Sigma-One
 * @created 19/04/2022 07:10
 **/

@Serializable
data class CSMMConfig(
    val gameDataPath: String = when {
        System.getProperty("os.name") == "Linux"                    -> File(System.getenv("HOME")).combineSafe(".local/share/Colossal Order/Cities_Skylines").toString()
        System.getProperty("os.name").startsWith("Windows") -> File(System.getenv("LOCALAPPDATA")).combineSafe("Colossal Order/Cities_Skylines").toString()
        // TODO: Support MacOS
        else -> throw UnsupportedOSException("CSMM is not yet supported on your operating system")
    }
)
