package backend.config

import io.ktor.util.*
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import util.CSMMJson
import util.Log
import util.UnsupportedOSException
import java.io.File
import java.io.FileNotFoundException
import kotlin.jvm.Throws

/** ConfigManager
 * @author  Sigma-One
 * @created 19/04/2022 06:32
 **/
object ConfigManager {
    var config: CSMMConfig? = null

    /**
     * Writes to or creates a CSMM configuration file, in a system default config location by default
     * Also updates currently loaded config to the newly written one
     * TODO: Override such that you can update only parts of the config easily
     * Linux  : $XDG_CONFIG_HOME/csmm/config.json (Fallback: $HOME/.config/csmm/config.json)
     * Windows: %LOCALAPPDATA%\csmm\config.json TODO: Verify that this nonsense works, I'm too lazy to boot my VM right now
     * MacOS  : TODO: Get a Hackintosh or something I guess, or get a MacOS user to handle this thing or something
     * @param   locationOverride        An override location for the config file
     * @throws  UnsupportedOSException  Thrown if location is not overridden and current OS has no defined default config location
     */
    @Throws(UnsupportedOSException::class)
    fun writeOrCreateConfig(newConfig: CSMMConfig, locationOverride: File? = null) {
        var configPath: File? = locationOverride
        if (configPath == null) {
            try { configPath = getSystemDefaultConfigPath() }
            catch (e: UnsupportedOSException) { throw e }
        }
        Log.INFO("Writing config path $configPath")

        configPath.parentFile.mkdirs()
        configPath.writeText(CSMMJson.encodeToString(newConfig))

        // Update config
        config = newConfig
    }

    /**
     * Loads a config from a file and updates the current config to it
     * @param   locationOverride        An override path to a non-default config file
     * @throws  FileNotFoundException   Thrown if a path is successfully found but the config was not found
     * @throws  UnsupportedOSException  Thrown if location is not overridden and current OS has no defined default config location
     */
    @Throws(
        FileNotFoundException::class,
        UnsupportedOSException::class
    )
    fun loadConfig(locationOverride: File? = null) {
        var configPath: File? = locationOverride
        if (configPath == null) {
            try { configPath = getSystemDefaultConfigPath() }
            catch (e: UnsupportedOSException) { throw e }
        }
        Log.INFO("Reading config path $configPath")

        config = Json.decodeFromString(configPath.readText())
    }

    @Throws(UnsupportedOSException::class)
    private fun getSystemDefaultConfigPath(): File {
        return when {
            System.getProperty("os.name") == "Linux" -> {
                //Log.INFO("Running Linux and config location not overridden, using defaults")
                // TODO: Handle horribly cursed systems where $HOME is missing
                // I know for almost certain someone will have something dumb like that
                // I'm also not sure I even want to know why
                if (System.getenv("XDG_CONFIG_HOME") == null) {
                    //Log.WARN("\$XDG_CONFIG_HOME not set, using secondary default")
                    File(System.getenv("HOME")).combineSafe(".config/csmm/config.json")
                } else {
                    // Handle systems where $XDG_CONFIG_HOME is not set for some reason
                    // Java File API '~' support when, this is ridiculous
                    File(
                        System.getenv("XDG_CONFIG_HOME").replace("~", System.getenv("HOME"))
                    ).combineSafe("csmm/config.json")
                }
            }

            System.getProperty("os.name").startsWith("Windows") -> {
                // I have absolutely no clue if this works
                File(System.getenv("LOCALAPPDATA")).combineSafe("csmm/config.json")
            }

            else -> {
                Log.FAIL("NOT IMPLEMENTED! Override config path or use a fully supported OS")
                Log.FAIL("Or implement it yourself, this thing is open source")
                Log.FAIL("https://framagit.org/Sigma-One/csmm-new")
                throw UnsupportedOSException("No default config path known for OS ${System.getProperty("os.name")}")
            }
        }
    }
}