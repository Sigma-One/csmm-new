package backend.data

import io.ktor.http.*
import java.nio.file.Path

/** DownloadContext
 * @author  Sigma-One
 * @created 09/04/2022 07:58
 **/
data class DownloadContext(
    var uuid        : String? = null,
    var isReady     : Boolean = false,
    var downloadURL : Url?    = null,
    var archive     : Path?   = null
)
