package backend.data

import io.ktor.util.*
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient
import java.io.File
import java.nio.file.Path
import kotlin.io.path.Path

/** ItemContext
 * @author  Sigma-One
 * @created 09/04/2022 07:53
 **/
@Serializable
data class ItemContext(
    var id             : Long?              = null,
    var title          : String?            = null,
    var description    : String?            = null,
    var authors        : ArrayList<String>? = null,
    var type           : ItemType?          = null,
    var updated        : Long?              = null,
    var dependencyIDs  : ArrayList<Long>?   = null,
    var fileSize       : Long?              = null,
    var isCollection   : Boolean            = false,
    @Transient
    var downloadContext: DownloadContext?   = null
) {
    /**
     * Returns a File pointing to the installation directory of this item
     * This requires the title and type to be set
     * @return  The installation directory
     */
    fun getDirectory(): File {
        // TODO: Load this from a config file
        val dataPath = Path("/home/sigma1/csmm-test")

        return dataPath.combineSafe(this.type!!.path).combineSafe(
            Path(this.title!!.map {
                if (it.isLetterOrDigit()) { it.lowercaseChar() }
                else { '_' }
            }.joinToString(""))
        )
    }

    override fun toString(): String {
        return "$type $id ($title)"
    }
}
