package backend.data

import java.nio.file.Path
import kotlin.io.path.Path

/** ItemType
 * @author  Sigma-One
 * @created 09/04/2022 07:56
 **/
enum class ItemType(val path: Path) {
    MOD(Path("/Addons/Mods/")),
    LUT(Path("/Addons/ColorCorrections/")),
    MAP(Path("/Maps/")),
    SAVE(Path("/Saves/")),
    STYLE(Path("/Addons/Styles/")),
    THEME(Path("/Addons/MapThemes/")),
    SCENARIO(Path("/Scenarios/")),
    ASSET(Path("/Addons/Assets/"));

    companion object {
        /**
         *  Returns an ItemType based on a string, which should represent a tag on the Steam Workshop
         *  @param  string  The tag string
         *  @return         ItemType based on the string. If not clear, defaults to ASSET
         */
        fun fromString(string: String): ItemType {
            return when(string.lowercase()) {
                "mod"                  -> MOD
                "lut"                  -> LUT
                "color correction lut" -> LUT
                "map"                  -> MAP
                "save"                 -> SAVE
                "style"                -> STYLE
                "district style"       -> STYLE
                "theme"                -> THEME
                "map theme"            -> THEME
                "scenario"             -> SCENARIO
                "asset"                -> ASSET
                // Lots of different asset tags on the workshop
                // Might separate some day idk
                // TODO: Also figure out where the cinematic cameras go, and add a field for them
                else -> ASSET
            }
        }
    }
}