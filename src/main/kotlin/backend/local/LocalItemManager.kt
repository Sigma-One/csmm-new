package backend.local

import backend.config.ConfigManager
import backend.data.ItemContext
import backend.data.ItemType
import backend.network.downloader.WorkshopDownloader
import io.ktor.util.*
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.decodeFromStream
import util.CSMMJson
import util.Log
import java.io.File
import java.io.FileNotFoundException

/** LocalItemManager
 * @author  Sigma-One
 * @created 12/04/2022 03:16
 **/
object LocalItemManager {
    /**
     * Creates (or overwrites) the context file for a provided manifest
     * @param   context     The context to write a manifest for
     */
    fun writeOrCreateManifest(context: ItemContext) {
        val file = context.getDirectory().combineSafe("manifest.json")

        file.writeText(Json.encodeToString(context))
    }

    /**
     * Gathers lists of all installed items. Ignores invalid items
     * @param   typeFilter  An ItemType to filter the results to, or null (default) if none
     * @return              A mapping of ItemTypes to ArrayLists of items
     */
    fun listItems(typeFilter: ItemType? = null): HashMap<ItemType, ArrayList<ItemContext>> {
        val lists = HashMap<ItemType, ArrayList<ItemContext>>()
        val dataPath = File(ConfigManager.config!!.gameDataPath)

        for (type in ItemType.values()) {
            val dirPath = dataPath.combineSafe(type.path)
            if (dirPath.listFiles() == null) { continue }
            lists[type] = ArrayList()
            for (dir in dirPath.listFiles()!!) {
                try {
                    val manifestFile = dir.combineSafe("manifest.json").inputStream()
                    // No IDEA I don't care if this is experimental, it compiles so it's fine
                    val manifest = CSMMJson.decodeFromStream<ItemContext>(manifestFile)
                    lists[type]!!.add(manifest)
                }
                catch (_: FileNotFoundException) {
                    Log.WARN("$dir does not contain a valid manifest")
                    continue
                }
            }
        }

        return lists
    }

    /**
     * Loads an ItemContext from a manifest file by item ID
     * @param   itemId  The ID of the item to load a context for
     * @return          The ItemContext loaded from a manifest, or null if not found
     */
    fun readManifestForID(itemId: Long): ItemContext? {
        // TODO: Optimise when not lazy
        // Then again I don't exactly care, we're in the 2020s and if you're running a C:S mod manager you can probably run C:S
        // And if you can run C:S, looping through at most a couple thousand things isn't too big
        // It's just a couple megabytes of RAM too, if you can't afford that then too bad (for now at least)
        // You should probably have like more than 16GB anyways if you're running C:S, at least with all the DLCs
        val lists = listItems()
        for (category in lists.values) {
            for (item in category) {
                if (item.id == itemId) {
                    return item
                }
            }
        }
        return null
    }

    /**
     * Removes the provided item and everything that depends on it
     * @param   item    Item to remove
     */
    fun removeItem(item: ItemContext) {
        // Yeet the dependents
        for (candidate in listItems().values.flatten()) {
            if (candidate.dependencyIDs!!.contains(item.id)) {
                removeItem(candidate)
            }
        }

        Log.INFO("Uninstalling item", item.id)
        item.getDirectory().deleteRecursively()
        Log.INFO("Item uninstalled", item.id)
    }

    /**
     * Checks for updates to items
     * @return  ArrayList of items that have updates available
     */
    fun checkForItemUpdates(): ArrayList<ItemContext> {
        val updateables = ArrayList<ItemContext>()
        val localItems  = listItems().values.flatten()
        for (item in localItems) {
            Log.INFO("Checking for updates", item.id)
            val newContext = ItemContext(item.id!!)
            // TODO: Check from workshop, that's probably faster, this is slow as hell
            runBlocking { WorkshopDownloader.requestInfoForItems(newContext) }
            if (newContext.updated!! > item.updated!!) {
                Log.INFO("Update available", item.id)
                updateables.add(item)
            }
            else {
                Log.INFO("No update needed", item.id)
            }
        }
        return updateables
    }
}