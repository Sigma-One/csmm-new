package backend.network.downloader

import kotlinx.serialization.Serializable

/** StatusRequestJson
 * @author  Sigma-One
 * @created 09/04/2022 10:36
 **/
@Serializable
data class StatusRequestJson(
    val uuids: List<String>
)