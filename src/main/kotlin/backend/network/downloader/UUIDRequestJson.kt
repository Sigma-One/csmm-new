package backend.network.downloader

import kotlinx.serialization.Required
import kotlinx.serialization.Serializable

/** UUIDRequestJson
 * @author  Sigma-One
 * @created 09/04/2022 10:27
 **/
@Serializable
data class UUIDRequestJson(
    val publishedFileId: Long,
    @Required val collectionId: Int? = null,
    @Required val hidden: Boolean = false,
    @Required val downloadFormat: String = "raw",
    @Required val autodownload: Boolean = false
)