package backend.network.downloader

import kotlinx.serialization.Serializable

/** UUIDResponseJson
 * @author  Sigma-One
 * @created 09/04/2022 10:19
 **/
@Serializable
data class UUIDResponseJson(
    val uuid: String
)