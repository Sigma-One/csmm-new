package backend.network.downloader

import backend.data.DownloadContext
import backend.data.ItemContext
import backend.data.ItemType
import backend.local.LocalItemManager
import backend.network.httpClient
import io.ktor.client.call.*
import io.ktor.client.request.*
import io.ktor.http.*
import io.ktor.util.*
import io.ktor.utils.io.*
import io.ktor.utils.io.core.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.*
import util.Log
import java.util.zip.ZipException
import java.util.zip.ZipFile
import kotlin.io.path.Path

/** WorkshopDownloader
 * @author  Sigma-One
 * @created 09/04/2022 10:13
 **/
object WorkshopDownloader {
    // Constants for URLs
    // We should probably check for the best node to use, currently hardcoded as 04
    // But it works so whatever
    private const val BASE_URL = "https://node04.steamworkshopdownloader.io/prod/api"
    private val UUID_REQUEST_URL = Url("$BASE_URL/download/request")
    private val STATUS_REQUEST_URL = Url("$BASE_URL/download/status")
    private val DETAILS_REQUEST_URL = Url("$BASE_URL/details/file")

    /**
     * Downloads and installs a set of items. Recurses to dependencies
     * @param   items           The items to install as ItemContext objects. Missing data will be filled in during execution.
     * @param   ignoreUpdates   Skip updating outdated items
     */
    suspend fun downloadItems(vararg items: ItemContext, ignoreUpdates: Boolean = false) {
        // Shadowed on purpose so we can modify
        @Suppress("NAME_SHADOWING")
        var items = arrayListOf(*items)

        // Initial info request
        requestInfoForItems(*items.toTypedArray())

        // Handle dependencies
        // This is another unholy mess but again I don't care as long as it works
        // The whole program should be rewritten in a non-JVM language anyways so the users don't need a JVM
        // Too bad, I only know Kotlin well enough
        // Somehow, however, this is cleaner than the first CSMM variants???
        // Don't even ask
        do {
            // Filter to items which' dependencies are no handled yet (the ID list is null)
            items.filter { it.dependencyIDs != null }.forEach { item ->
                Log.INFO("Scanning dependencies", item.id)
                val newItems = ArrayList<ItemContext>()
                // Create a context for each new item
                item.dependencyIDs!!.forEach { id ->
                    Log.INFO("Found dependency $id", item.id)
                    newItems.add(ItemContext(id))
                }
                // Request info for new items, refreshing their dependencies, then add to main items list
                requestInfoForItems(*newItems.toTypedArray())
                items += newItems
            }
        } while (items.any { it.dependencyIDs == null })
        // Remove duplicates
        items = ArrayList(items.distinct())

        // Ignore already installed items unless there is an update
        // Obviously also ignore if ignoreUpdates is set
        // Yes it's cursed
        // Yes it's also another instance of me not caring if it's cursed
        val installed = LocalItemManager.listItems().values.flatten()
        items.filter { item -> installed.map { it.id }.contains(item.id) }.forEach { pendingItem ->
            if (
                ignoreUpdates
            ||  pendingItem.updated!! <= installed.find { it.id == pendingItem.id }!!.updated!!
            ) {
                Log.INFO("Item already installed and no updates available or requested, ignoring item", pendingItem.id)
                items.remove(pendingItem)
            }
        }

        if (items.size == 0) {
            Log.INFO("No items will be installed, exiting")
            return
        }

        // Fetch UUIDs for each item
        // Could this be in parallel with status checking? Yes
        // Do I care? No
        coroutineScope {
            for (item in items) {
                item.downloadContext = DownloadContext()
                launch { requestUUIDForItem(item) }
            }
        }

        Log.INFO("All UUIDs received, polling file status")

        // Poll for download status of each item
        while (items.any { !it.downloadContext!!.isReady }) {
            val statuses = Json.parseToJsonElement(httpClient.post(STATUS_REQUEST_URL) {
                contentType(ContentType.Application.Json)
                setBody(StatusRequestJson(items.map { it.downloadContext!!.uuid!! }))
            }.body())

            coroutineScope {
                for (item in items) {
                    // I have no idea if this actually NPEs
                    // But it did in the version this is inherited from
                    // I don't know why, I don't want to know why, and I don't even really care why
                    // All I know is that we can just skip that lööp iteration
                    try {
                        item.downloadContext!!.isReady = statuses
                            .jsonObject[item.downloadContext!!.uuid]!!
                            .jsonObject["progress"]!!
                            .jsonPrimitive.int == 100
                    } catch (_: NullPointerException) {
                        Log.WARN("Status check failed", item.id)
                        continue
                    }

                    // We can use the download URL to check if the item has entered next stage of processing
                    // Yes it's a bit ugly, too bad
                    if (item.downloadContext!!.isReady && item.downloadContext!!.downloadURL == null) {
                        // Build download URL
                        item.downloadContext!!.downloadURL = Url(
                            "https://"
                                + statuses
                                .jsonObject[item.downloadContext!!.uuid]!!
                                .jsonObject["storageNode"]!!
                                .jsonPrimitive.content
                                + "/prod/storage/"
                                + statuses
                                .jsonObject[item.downloadContext!!.uuid]!!
                                .jsonObject["storagePath"]!!
                                .jsonPrimitive.content
                                + "?uuid="
                                + item.downloadContext!!.uuid
                        )

                        // Call next step in process
                        Log.INFO("File is ready, proceeding to actual download", item.id)
                        launch { installModFiles(item) }
                    }
                }
            }
        }
    }

    private suspend fun requestUUIDForItem(item: ItemContext) {
        Log.INFO("Requesting UUID", item.id)

        val response = httpClient.post(UUID_REQUEST_URL) {
            contentType(ContentType.Application.Json)
            setBody(UUIDRequestJson(item.id!!))
        }

        // We know this is non-null as it is private and only called in downloadItems
        item.downloadContext!!.uuid = Json.decodeFromString<UUIDResponseJson>(response.body()).uuid

        Log.INFO("UUID Received (${item.downloadContext!!.uuid})", item.id)
    }

    /**
     * Fills item info with that available from the downloader service
     * @param   items   Vararg of ItemContexts to fill info for
     */
    suspend fun requestInfoForItems(vararg items: ItemContext) {
        val result = Json.parseToJsonElement(httpClient.post(DETAILS_REQUEST_URL) {
            setBody("[${items.map{ it.id }.joinToString(", ")}]")
        }.body())

        // Parse resulting JSON array and gather data from there, I hate this
        // Webdevs fucking use a properly typed language for once
        // So you'd maybe understand why A BARE ARRAY IS NOT A GOOD IDEA IN YOUR JSON-RETURNING API
        // I wish I could just use a data class like all the other static template based JSON parsing
        // But no, first off the JSON spec is crap, and secondly webshite
        result.jsonArray.forEach { block ->
            val item = items.filter { it.id == block.jsonObject["publishedfileid"]!!.jsonPrimitive.content.toLong() }[0]
            item.title       = block.jsonObject["title"           ]!!.jsonPrimitive.content
            item.fileSize    = block.jsonObject["file_size"       ]!!.jsonPrimitive.content.toLong()
            item.description = block.jsonObject["file_description"]!!.jsonPrimitive.content
            item.updated     = block.jsonObject["time_updated"    ]!!.jsonPrimitive.int.toLong()
            item.type        = ItemType.fromString(block
                .jsonObject["tags"]!!
                .jsonArray[0]
                .jsonObject["tag"]!!
                .jsonPrimitive
                .content
            )
            // This is a mess
            // Too bad, I don't care
            item.dependencyIDs = if(block.jsonObject["num_children"]!!.jsonPrimitive.int < 1) { arrayListOf() }
            else { ArrayList(block.jsonObject["children"]!!.jsonArray.map { child ->
                child.jsonObject["publishedfileid"]!!.jsonPrimitive.long
            })}
        }
    }

    private suspend fun installModFiles(item: ItemContext) {
        // Generate archive path and file
        val archive = item.getDirectory().combineSafe("archive.zip")

        archive.parentFile.mkdirs()
        item.downloadContext!!.archive = archive.toPath()

        httpClient.prepareGet(item.downloadContext!!.downloadURL!!).execute { response ->
            val byteChannel = response.body<ByteReadChannel>()

            // TODO: Event bussy thing for download progress reporting?
            while (!byteChannel.isClosedForRead) {
                val packet = byteChannel.readRemaining(DEFAULT_BUFFER_SIZE.toLong())
                while (packet.isNotEmpty) {
                    archive.appendBytes(packet.readBytes())
                }
            }
        }

        Log.INFO("Download complete, extracting", item.id)

        // The unzip pyramid of Giza
        try {
            withContext(Dispatchers.IO) { ZipFile(archive) }.use { zip ->
                for (entry in zip.entries().asSequence()) {
                    zip.getInputStream(entry).use { input ->
                        val entryFile = archive.parentFile.combineSafe(Path(entry.name))
                        if (entry.isDirectory) { entryFile.mkdirs() }
                        else {
                            entryFile.outputStream().use { output ->
                                input.copyTo(output)
                            }
                        }
                    }
                }
            }
            Log.INFO("Installation complete", item.id)
        }
        catch (err: ZipException) {
            Log.FAIL("Extraction failed", item.id)
        }

        LocalItemManager.writeOrCreateManifest(item)

        // TODO: Send status to frontend somehow
        archive.delete()
    }
}