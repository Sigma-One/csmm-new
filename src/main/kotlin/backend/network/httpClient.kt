package backend.network

import io.ktor.client.*
import io.ktor.client.plugins.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.serialization.kotlinx.json.*

/** httpClient
 * @author  Sigma-One
 * @created 09/04/2022 10:11
 **/
val httpClient = HttpClient() {
    install(ContentNegotiation) {
        json()
    }
    install(UserAgent) {
        agent = "CSMM"
    }
}

/*
 * Please do not report or remove Sigrun
 * She has permission to be here
 * - SINEWAVE

                                    \     /
                                     \___/
                                    (0) (0)
                                     \   /
                                    / '·'
                                  /   /
                                 /    ||
                                /   . <|
                               /   ,  ||      .-
                              /   /  / /.- ,'  |
                              /  /  / .' |' .< |
                             /  /  /.' < |.' < |
                            /  /     ,'< |   < |
                           /_  /| _,'  < |    <'
                     -,  .'  '-' ''     <'
                  ../  \/   _  \
                 /| \\.'   | || |
                 | \ \   _ | || |
                 '| \ \ | || |L ''-.,_
                /||  \ \| |  *,''-.__. '',
              ,'  '|, \._.) |  ''-..'=:: )
             /  ,' ||      /       '-_) |
           ,'  /   '|,     ;        | / |
          /  ,'     ||    /         || ||
        ,'  /       '|,  /          || ||
       / , '          ||  ;         || ||
     ,' /            '|,/#_         ||  \=>
    /, '            _,||            ||
   //_______====--''' _#_           \=>

 */

