package backend.network.workshop

import backend.data.ItemContext
import backend.network.httpClient
import io.ktor.client.call.*
import io.ktor.client.request.*
import io.ktor.http.*
import kotlinx.serialization.json.*
import kotlinx.serialization.json.JsonNull.content
import util.CthulhuRlyehWgahnaglFhtagnException
import java.lang.IndexOutOfBoundsException
import kotlin.jvm.Throws

/** SteamWorkshop
 * @author  Sigma-One
 * @created 20/04/2022 07:44
 * WARNING: This file contains horrible cursed HTML parsing nightmares
 *          Proceed at your own risk
 *          I deny all responsibility for any summonings of eldritch nightmares
 *
 * TL;DR  : Here be dragons
 **/
object SteamWorkshop {
    private const val BASE_URL           = "https://steamcommunity.com"
    private const val WORKSHOP_ITEM_URL  = "$BASE_URL/sharedfiles/filedetails?id="
    private const val WORKSHOP_QUERY_URL = "$BASE_URL/workshop/browse/?appid=255710&searchtext="

    /**
     * Fills item info with that available from the downloader service
     * This is kind of redundant with backend.network.downloader.WorkshopDownloader::requestInfoForItems
     * But whatever, this does get some info the downloader one doesn't, such as item authors
     * However, because things like getting the creation date is janky here, you should ideally call both this and
     * the workshopDownloader variant, even though that is slow
     * In fact, currently this only gets the author, because everything else can be handled by that one
     * But this remains as a "general" info fetching method, because we may need more in the future
     * @param   item   ItemContext to fill info for
    */
    suspend fun requestInfoForItem(item: ItemContext) {
        val htmlContent = httpClient.get(WORKSHOP_ITEM_URL+item.id).body<String>()

        item.authors = ArrayList(".*<br>".toRegex().findAll(content // Find line break tags in contet block filtered out below
            .substringAfter("creatorsBlock\">") // Locate panel containing the creators
            .substringBefore("<div class=\"panel") // Limit to approximately panel contents
        ).map { result ->
            result.value
                .strip() // Remove leading whitespace from indentation
                .substringBeforeLast("<br>") // Remove line break tag from end
        }.toList())
    }

    /**
     * Searches the workshop for a keyword and returns a list of ItemContexts found
     * NOTE: This only fills in the primary author to speed things up, call requestInfoForItem for full list
     * NOTE: Also only fills in a partial description, call requestInfoForItem for full info; TODO: Implement this in requestInfoForItem
     * TODO: Filtering, sorting(?), etc.
     * WARNING: This code is particularly bad and cursed and I hate it
     * @param   query   Workshop query String
     * @param   page    Workshop page number, defaults to first
     * @return          ArrayList of found ItemContexts, limited to one page
     * @throws  CthulhuRlyehWgahnaglFhtagnException  HTML tags lea͠ki̧n͘g fr̶ǫm ̡yo͟ur eye͢s̸ ̛l̕ik͏e liquid pain
     */
    @Throws(CthulhuRlyehWgahnaglFhtagnException::class)
    suspend fun keywordQuery(query: String, page: Int = 1): ArrayList<ItemContext> {
        val htmlContent = httpClient.get(Url("$WORKSHOP_QUERY_URL$query&p=$page")).body<String>()

        try {
            val blocks = ArrayList(htmlContent
                .split("workshopBrowseItems\">")[1]
                .split("<div class=\"workshopBrowsePaging")[0]
                .replace("[\r\n\t]".toRegex(), "")
                .splitToSequence("</script>")
                .toList()
                .dropLast(1)
                .map {
                    Pair(
                        it.split(
                            "?appid=255710\">",
                            limit = 2
                        )[1].replace("</a></div>.*".toRegex(), ""),
                        Json.parseToJsonElement(it
                            .split("?appid=255710\">", limit = 2)[1]
                            .replace("SharedFileBindMouseHover( ", "")
                            .replace(".*\"sharedfile_[0-9]+\", false, ".toRegex(), "")
                            .split(");")[0]
                        )
                    )
                }
            )

            return ArrayList(blocks.map { block ->
                ItemContext(
                    id = block.second.jsonObject["id"]!!.jsonPrimitive.content.toLong(),
                    title = block.second.jsonObject["title"]!!.jsonPrimitive.content,
                    description = block.second.jsonObject["description"]!!.jsonPrimitive.content,
                    authors = arrayListOf(block.first)
                )
            })
        }
        // If index is out of bounds there are no results so parsing fails, and we should return an empty ArrayList
        catch (_: IndexOutOfBoundsException) {
            return arrayListOf()
        }
        catch (e : Throwable) {
            throw CthulhuRlyehWgahnaglFhtagnException(e)
        }
    }
}