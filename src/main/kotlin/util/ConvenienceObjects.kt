package util

import kotlinx.serialization.json.Json

/** ConvenienceObjects
 * @author  Sigma-One
 * @created 19/04/2022 08:09
 **/
val CSMMJson = Json {
    prettyPrint       = true
    ignoreUnknownKeys = true
}