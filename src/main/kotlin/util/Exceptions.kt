package util

/** Exceptions
 * @author  Sigma-One
 * @created 19/04/2022 12:29
 **/

class UnsupportedOSException(message: String): Exception(message)
class CthulhuRlyehWgahnaglFhtagnException(val source: Throwable)  : Exception()