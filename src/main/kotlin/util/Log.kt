package util

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

object Log {
    private val ESC = Char(0x1b)
    private val dateFormatter = DateTimeFormatter.ofPattern("HH:mm:ss")

    fun INFO(string: String) {
        println("$ESC[42;1;30m INFO $ESC[0;1m ${LocalDateTime.now().format(dateFormatter)}$ESC[0m $string")
    }

    fun INFO(string: String, id: Long?) {
        INFO("[ ${ id.toString().padEnd(10, ' ') } ] $string")
    }

    fun WARN(string: String) {
        println("$ESC[43;1;30m WARN $ESC[0;1m ${LocalDateTime.now().format(dateFormatter)}$ESC[0m $string")
    }

    fun WARN(string: String, id: Long?) {
        WARN("[ ${ id.toString().padEnd(10, ' ') } ] $string")
    }

    fun FAIL(string: String) {
        println("$ESC[41;1;30m FAIL $ESC[0;1m ${LocalDateTime.now().format(dateFormatter)}$ESC[0m $string")
    }

    fun FAIL(string: String, id: Long?) {
        FAIL("[ ${ id.toString().padEnd(10, ' ') } ] $string")
    }
}